>voir sur [Dillinger](https://dillinger.io/) pour apprécier la beauté du Markdown =)

# Whiteboard

A tool to post-it your ideas.

___
## Sommaire
* Compilation & Exécution
* Présentation du projet
___

## Compilation & Exécution

### Pré-requis

* Être dans le root du dossier
* Make doit être installé
* gcc doit être installé


### Compilation

* Compiler tout le projet : `make `

* Compiler le server: `make server`

* Compiler le client: `make client`

* Clear les fichiers binaires: `make clear`

### Exécution

* Serveur : `./bin/setup` (une fois au début !)

* Client : `./bin/client`(autant de fois que vous voulez)
___
## Présentation du projet

Ce programme simule un whiteboard, c'est à dire un tableau où **n** processus peuvent déposer et modifier des *Post-It*.
Chaque processus client possède un thread qui attend la mise à jour du whiteboard. Lorsqu'elle survient, le thread affiche le nouveau whiteboard et se met à nouveau en attente.
> *NB* : Par défaut, le nombre max de Post-It est fixé à **10** pour optimiser la visibilité du terminal, mais vous pouvez adapter ce nombre via la macro `WHITEBOARD_SIZE` du fichier source `ressources.c`