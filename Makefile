all:src/setup.c src/client.c
	gcc src/setup.c -lpthread -o bin/setup
	gcc src/client.c -lpthread -o bin/client
	$(info --SETUP IPCS >> ./bin/setup)

clear:
	rm bin/*