#include "ressources.c"

int main(){
	//--------clef--------------
	key_t clef = ftok("src/.key",0);

	if(clef < 0 ){
		perror("ftok : ");
		return 1;
	}
	
	
	printf("Key : %d\n",clef);


	//-----Segment-partagé---------
	int shmID = shmget(clef, sizeof(whiteboard), IPC_CREAT|0666);

	if(shmID < 0 ){
		perror("shmget : ");
		return 1;
	}

	printf("Segment partagé %d créé (%ld ko)\n",shmID,sizeof(whiteboard)/1000);


	//-------Tableau-de-sémaphore-------------
	int idSemArray = semget(clef, WHITEBOARD_SIZE + 1, IPC_CREAT|0666);

	if(idSemArray < 0 ){
		perror("semget : ");
		return 1;
	}

	print_whiteboard(shmID,idSemArray);

	//listen_event(shmID,idSemArray);

	pthread_t eventlistenerID;

	char current[100];

	event_param p;
	p.shm = shmID;
	p.sem = idSemArray;
	p.current = current;

	if(pthread_create(&eventlistenerID,NULL,listen_event,(void*)&p)<0){
		perror("pthread_create : ");
		return 1;
	}

	char line[DESCRIPTION_SIZE];
	char titre_note[TITLE_SIZE];
	char description_note[DESCRIPTION_SIZE];
	int num_note=9;
	int c;
	while(1){
//memset(line1,0,sizeof line1);
		memset(line,0,sizeof line);

		strcpy(current,"Quelle note voulez-vous modifier ? >");

		do{

			printf("%s",current);
			scanf("%d",&num_note);

			flush_stdin();
		}while(WHITEBOARD_SIZE -1 < num_note || num_note < 0);


		strcpy(current,"Nouveau titre de la note >");

		printf("%s",current);
		fgets(line,sizeof line,stdin);

		strcpy(titre_note,strtok(line,"\n"));

		//flush_stdin();

		strcpy(current,"Nouvelle description de la note >");

		printf("%s",current);

		fgets(line,sizeof line,stdin);

		strcpy(description_note,strtok(line,"\n"));

		//flush_stdin();

		change_note(shmID,idSemArray,num_note,titre_note,description_note);

		//sleep(1);

	}

	return 0;
}