#include <errno.h>
#include <sys/msg.h> //Files de messages
#include <sys/types.h>
#include <sys/ipc.h>
#include <stdio.h> //printf, scanf...
#include <stdlib.h> //malloc, atoi, rand... 
#include <unistd.h> //sleep,getpid
#include <time.h> //time
#include <sys/shm.h>//shmget
#include <semaphore.h>//semaphore
#include <sys/sem.h> //semaphore arrays
#include <string.h> // strcpy
#include <pthread.h>

#define TITLE_SIZE 20
#define DESCRIPTION_SIZE 180 

typedef struct note{
	char titre[TITLE_SIZE];
	char description[DESCRIPTION_SIZE];
}note;


#define WHITEBOARD_SIZE 10

typedef struct whiteboard{
	note board[WHITEBOARD_SIZE];
	int note_number;
}whiteboard;

typedef struct event_param{
	int shm;
	int sem;
	char* current;
}event_param;


note init_note(char* titre,char* description){
	note n;
	if(titre == NULL)
		strcpy(n.titre,"Bonjour");
	else
		strcpy(n.titre,titre);

	if(description == NULL)
		strcpy(n.description,"Je m'appelle Henry, je voudrais bien réussir ma vie");
	else
		strcpy(n.description,description);

	return n;
}

void print_note(note* n){
	printf("Titre : %s \nDescription : %s\n\n",n->titre,n->description);
}

void print_whiteboard(int shm,int sem){
	//----opérations-sur-les-sémaphores
	struct sembuf P;
	P.sem_op  = -1;
	P.sem_flg = 0;

	struct sembuf V;
	V.sem_op  = 1;
	V.sem_flg = 0;

	note n;

	printf("-----WHITEBOARD-----\n");
	for(int i=1; i<WHITEBOARD_SIZE +1; i++){
		P.sem_num = i;
		V.sem_num = i;

		semop(sem,&P,1);
		whiteboard* board = shmat(shm,NULL,0);

		if(board == NULL){
			perror("Whiteboard not found : ");
			return;
		}

		n = board->board[i-1];
		printf("|Post-It %d|\n",i-1);
		print_note(&n);

		semop(sem,&V,1);
	}
}

void change_note(int shm,int sem,int note_nbr,char* title,char* desc){
		//----opérations-sur-les-sémaphores
	struct sembuf P;
	P.sem_op  = -1;
	P.sem_flg = 0;
	P.sem_num = note_nbr +1;

	struct sembuf V;
	V.sem_op  = 1;
	V.sem_flg = 0;
	V.sem_num = note_nbr +1;

	note n;

	if(semop(sem,&P,1)<0){
		perror("Semaphore P error : ");
		return;
	}


	whiteboard* board = shmat(shm,NULL,0);

	if(board == NULL){
		perror("Whiteboard not found : ");
		return;
	}


	if(title == NULL)
		strcpy(board->board[note_nbr].titre,"Undefined");
	else
		strcpy(board->board[note_nbr].titre,title);

	if(desc == NULL)
		strcpy(board->board[note_nbr].description,"Undefined");
	else
		strcpy(board->board[note_nbr].description,desc);

	if(semop(sem,&V,1)<0){
		perror("Semaphore V error : ");
		return;
	}

	P.sem_num = 0;
	V.sem_num = 0;

	if(semop(sem,&P,1)<0){
		perror("Semaphore P error : ");
		return;
	}
	if(semop(sem,&V,1)<0){
		perror("Semaphore V error : ");
		return;
	}

}

void flush_stdin(){
	int c;
	while ((c = getchar()) != '\n' && c != EOF);
}

void* listen_event(void* param){
	event_param p = *(event_param*)param;

	struct sembuf Z;
	Z.sem_op  = 0;
	Z.sem_flg = 0;
	Z.sem_num = 0;

	while(1){
		if(semop(p.sem,&Z,1)<0){
			perror("Semaphore Z error : ");
			return NULL;
		}
		system("clear");
		print_whiteboard(p.shm,p.sem);
		printf("%s",p.current);
		fflush(stdout);
	}

	printf("Fin d'écoute d'event.\n");
	return NULL;
}
