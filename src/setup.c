#include "ressources.c"

int main(){

	//--------clef--------------
	key_t clef = ftok("src/.key",0);

	if(clef < 0 ){
		perror("ftok : ");
		return 1;
	}
	


	//-----Segment-partagé---------
	int shmID = shmget(clef, sizeof(whiteboard), IPC_CREAT|0666);

	if(shmID < 0 ){
		perror("shmget : ");
		return 1;
	}

	printf("Segment partagé %d créé (%ld ko).\n",shmID,sizeof(whiteboard)/1000);

	whiteboard* board = shmat(shmID,NULL,0);

	if(board == NULL){
		perror("Whiteboard not found : ");
		return 1;
	}

	board->note_number = WHITEBOARD_SIZE;

	if(shmdt(board)<0){
		perror("Error during whiteboard detachment");
		return 1;
	}


	//-------Tableau-de-sémaphore-------------
	int idSemArray = semget(clef, WHITEBOARD_SIZE + 1, IPC_CREAT|0666);

	if(idSemArray < 0 ){
		perror("semget : ");
		return 1;
	}

	printf("Tableau de sémaphore %d créé.\n",idSemArray);


	for(int i=0;i < WHITEBOARD_SIZE +1 ; i++){
		if(semctl(idSemArray, i , SETVAL, 1)<0){
			perror("semctl : ");
			return 1;
		}
	}

	printf("Exécuter un processus client : ./bin/client\n");

	return 0;
}